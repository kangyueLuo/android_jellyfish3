package com.jellyfish.ar

import com.badlogic.gdx.files.FileHandle
import com.badlogic.gdx.graphics.Pixmap

interface CameraControl {

    val pictureData : ByteArray
    val isReady : Boolean

    fun prepareCamera()

    fun startPreview()
    fun stopPreview()
    fun takePicture()

    fun startPreviewAsync()
    fun stopPreviewAsync()
    fun prepareCameraAsync()
    fun takePictureAsync(timeout: Long): ByteArray

    fun saveAsJpeg(jpgfile: FileHandle, cameraPixmap: Pixmap)


}