package com.jellyfish.ar

import com.badlogic.gdx.Application
import com.badlogic.gdx.ApplicationAdapter
import com.badlogic.gdx.Gdx
import com.badlogic.gdx.graphics.GL20
import com.badlogic.gdx.graphics.PerspectiveCamera
import com.badlogic.gdx.graphics.Texture
import com.badlogic.gdx.graphics.g2d.SpriteBatch
import com.badlogic.gdx.scenes.scene2d.Stage
import com.badlogic.gdx.utils.viewport.StretchViewport
import java.text.SimpleDateFormat

class App(private val cameraControl: CameraControl?) : ApplicationAdapter() {
    private val TAG = "App"
    private var stage: Stage? = null

    lateinit var batch: SpriteBatch
    private val sDateFormat = SimpleDateFormat("yyyy-MM-dd hh:mm:ss")
    private val date = sDateFormat.format(java.util.Date())

    private var gmaeCamera: PerspectiveCamera? = null

    // set mode neomal
    private var mode: Mode? = null



    // Camera state
    enum class Mode {
        normal, prepare, preview, takePicture, waitForPictureReady
    }


    override fun create() {
        // set Log LEVEL
        Gdx.app.logLevel = Application.LOG_DEBUG
        // use StretchViewport create stage

        stage = Stage(StretchViewport(WORLD_WIDTH, WORLD_HEIGHT))
        // set stage catch input
        Gdx.input.inputProcessor = stage

        // set mode to normal
        mode = Mode.normal

        // control camera in normal mode
        if (mode == Mode.normal) {
            mode = Mode.prepare
            cameraControl?.prepareCameraAsync()


        }

        batch = SpriteBatch()

        // Config Game camera
        gmaeCamera = PerspectiveCamera(67.0f,
                2.0f * Gdx.graphics.width / Gdx.graphics.height, 2.0f)
        gmaeCamera!!.far = 100.0f
        gmaeCamera!!.near = 0.1f
        gmaeCamera!!.position.set(2.0f, 2.0f, 2.0f)
        gmaeCamera!!.lookAt(0.0f, 0.0f, 0.0f)
    }

    override fun render() {
        Gdx.gl20.glHint(GL20.GL_GENERATE_MIPMAP_HINT, GL20.GL_NICEST)

        // switch mode to control camera
        when (mode) {
            App.Mode.normal -> {
                // clear screen to blue.
                Gdx.gl20.glClearColor(0.0f, 0.0f, 0.6f, 1.0f)
            }
            App.Mode.prepare -> {
                // clear screen to half translucent （半透明）.
                Gdx.gl20.glClearColor(0.0f, 0.0f, 0f, 0.6f)
                if (cameraControl != null) {
                    if (cameraControl.isReady) {

                        // Async control camera to preview
                        cameraControl.startPreviewAsync()

                        // set mode to preview
                        mode = Mode.preview
                    }
                }
            }
            App.Mode.preview -> {
                // 完全透明，可以看到 攝影機的影像
                Gdx.gl20.glClearColor(0.0f, 0.0f, 0.0f, 0f)
            }
//            App.Mode.takePicture -> {
//                // clear screen to trasnparent
//                Gdx.gl20.glClearColor(0f, 0.0f, 0.0f, 0.0f)
//                cameraControl?.takePicture()
//                // set to waitForPictureReady mode
//                mode = Mode.waitForPictureReady
//            }
//            App.Mode.waitForPictureReady -> {
//                Gdx.gl20.glClearColor(0.0f, 0f, 0.0f, 0.0f)
//            }
        }

        Gdx.gl20.glClear(GL20.GL_COLOR_BUFFER_BIT or GL20.GL_DEPTH_BUFFER_BIT)

        batch.begin()
        stage!!.act()   // 更新舞台逻辑
        stage!!.draw()  // 绘制舞台
        batch.end()

        Gdx.gl20.glEnable(GL20.GL_DEPTH_TEST)
        Gdx.gl20.glEnable(GL20.GL_TEXTURE)
        Gdx.gl20.glEnable(GL20.GL_TEXTURE_2D)
        Gdx.gl20.glEnable(GL20.GL_LINE_LOOP)
        Gdx.gl20.glDepthFunc(GL20.GL_LEQUAL)
        Gdx.gl20.glClearDepthf(1.0f)
        gmaeCamera!!.update(true)

    }

    override fun dispose() {
        if (stage != null) {
            stage!!.dispose()
        }


        batch.dispose()
    }

    companion object {

        //（StretchViewport） to view stage
        val WORLD_WIDTH = 480f
        val WORLD_HEIGHT = 800f
    }
}
