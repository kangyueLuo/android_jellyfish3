package com.yue.ar.coffee.camera

import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.drawable.Drawable
import android.hardware.Camera
import android.util.Log
import android.view.ViewGroup
import android.view.ViewParent
import android.view.Window
import android.view.WindowManager
import android.widget.RelativeLayout

import com.badlogic.gdx.files.FileHandle
import com.badlogic.gdx.graphics.Pixmap
import com.jellyfish.ar.AndroidLauncher
import com.jellyfish.ar.CameraControl
import com.jellyfish.ar.camera.CameraSurface

import java.io.ByteArrayOutputStream

import java.io.FileNotFoundException
import java.io.FileOutputStream
import java.io.IOException



class AndroidCameraController(private val activity: AndroidLauncher) : CameraControl, Camera.PictureCallback, Camera.AutoFocusCallback, Camera.PreviewCallback{

    private val TAG = "AndroidCameraController"
    private var cameraSurface: CameraSurface? = null
    override lateinit var pictureData: ByteArray

    val statusBarHeight: Int
        get() {
            var result = 0
            val resourceId = activity.resources.getIdentifier("status_bar_height", "dimen", "android")
            if (resourceId > 0) {
                result = activity.resources.getDimensionPixelSize(resourceId)
            }
            return result
        }

    override val isReady: Boolean
        get() = if (cameraSurface != null && cameraSurface!!.camera != null) {
            true
        } else false

    internal var shutterCallback: Camera.ShutterCallback = Camera.ShutterCallback { }

    internal var jpegPictureCallback: Camera.PictureCallback = Camera.PictureCallback { data, arg1 -> pictureData = data }


    override fun prepareCamera() {
        if (cameraSurface == null) {
            cameraSurface = CameraSurface(activity, this)
        }

        activity.addContentView(cameraSurface, RelativeLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT))
        try {
            activity.requestWindowFeature(Window.FEATURE_NO_TITLE)
        } catch (ex: Exception) {
            ex.printStackTrace()
        }

        activity.window.clearFlags(WindowManager.LayoutParams.FLAG_FORCE_NOT_FULLSCREEN)
        activity.window.clearFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN)


    }

    override fun startPreview() {
        if (cameraSurface != null && cameraSurface!!.camera != null) {
            cameraSurface!!.camera!!.startPreview()
        }
    }

    override fun stopPreview() {

        if (cameraSurface != null) {
            val parentView = cameraSurface!!.parent
            if (parentView is ViewGroup) {
                parentView.removeView(cameraSurface)
            }
            if (cameraSurface!!.camera != null) {
                cameraSurface!!.camera!!.stopPreview()
            }
        }

        activity.restoreFixedSize()

    }

    fun setCameraParametersForPicture(camera: Camera?) {
        // Before we take the picture - we make sure all camera parameters are
        // as we like them
        // Use max resolution and auto focus

        val p = camera!!.parameters
        val supportedSizes = p.supportedPictureSizes
        var maxSupportedWidth = -1
        var maxSupportedHeight = -1
        for (size in supportedSizes) {
            if (size.width > maxSupportedWidth) {
                maxSupportedWidth = size.width
                maxSupportedHeight = size.height
            }
        }
        p.setPictureSize(maxSupportedWidth, maxSupportedHeight)
        p.focusMode = Camera.Parameters.FOCUS_MODE_AUTO
        camera.parameters = p
    }

    override fun takePicture() {
        // the user request to take a picture - start the process by requesting
        // focus
        setCameraParametersForPicture(cameraSurface!!.camera)
        cameraSurface!!.camera!!.autoFocus(this)
    }

    override fun startPreviewAsync() {
        val r = Runnable { startPreview() }
        activity.post(r)
    }

    override fun stopPreviewAsync() {
        val r = Runnable { stopPreview() }
        activity.post(r)
    }

    override fun prepareCameraAsync() {
        val r = Runnable { prepareCamera() }
        activity.post(r)
    }

    override fun takePictureAsync(timeout: Long): ByteArray {
        var timeout = timeout
        timeout *= ONE_SECOND_IN_MILI.toLong()

        val r = Runnable { takePicture() }
        activity.post(r)
        while (pictureData == null && timeout > 0) {
            try {
                Thread.sleep(ONE_SECOND_IN_MILI.toLong())
                timeout -= ONE_SECOND_IN_MILI.toLong()
            } catch (e: InterruptedException) {
                // TODO Auto-generated catch block
                e.printStackTrace()
            }

        }
        if (pictureData == null) {
            cameraSurface!!.camera!!.cancelAutoFocus()
        }
        return this!!.pictureData!!
    }

    override fun saveAsJpeg(jpgfile: FileHandle, pixmap: Pixmap) {
        val fos: FileOutputStream
        var x = 0
        var y = 0
        var xl = 0
        var yl = 0
        try {
            val bmp = Bitmap.createBitmap(pixmap.width,
                    pixmap.height, Bitmap.Config.ARGB_8888)
            // we need to switch between LibGDX RGBA format to Android ARGB
            // format
            x = 0
            xl = pixmap.width
            while (x < xl) {
                y = 0
                yl = pixmap.height
                while (y < yl) {
                    val color = pixmap.getPixel(x, y)
                    // RGBA => ARGB
                    val RGB = color shr 8
                    val A = color and 0x000000ff shl 24
                    val ARGB = A or RGB
                    bmp.setPixel(x, y, ARGB)
                    y++
                }
                x++
            }

            fos = FileOutputStream(jpgfile.file())
            bmp.compress(Bitmap.CompressFormat.JPEG, 90, fos)
            fos.flush()
            fos.close()
        } catch (e: FileNotFoundException) {
            e.printStackTrace()
        } catch (e: IOException) {
            e.printStackTrace()
        } catch (e: IllegalArgumentException) {
            e.printStackTrace()
        }

    }

    override fun onAutoFocus(success: Boolean, camera: Camera?) {
        // Focus process finished, we now have focus (or not)
        if (success) {
            if (camera != null) {

                camera.takePicture(shutterCallback, null, jpegPictureCallback)

                camera.startPreview()

            }
        }
    }

    override fun onPictureTaken(pictureData: ByteArray, camera: Camera) {
        this.pictureData = pictureData
    }


    override fun onPreviewFrame(previewData: ByteArray, camera: Camera) {
        
    }

    companion object {

        private val ONE_SECOND_IN_MILI = 1000
    }

}
