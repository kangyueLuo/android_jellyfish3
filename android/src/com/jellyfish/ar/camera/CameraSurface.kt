package com.jellyfish.ar.camera

import android.content.Context
import android.hardware.Camera
import android.view.SurfaceHolder
import android.view.SurfaceView
import com.yue.ar.coffee.camera.AndroidCameraController

class CameraSurface(context: Context, cameraController: AndroidCameraController) : SurfaceView(context), SurfaceHolder.Callback {

    var camera: Camera? = null
        private set

    internal var cameraController: AndroidCameraController? = null

    init {

        this.cameraController = cameraController

        holder.addCallback(this)
        holder.setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS)
    }

    override fun surfaceCreated(surfaceHolder: SurfaceHolder) {
        camera = Camera.open(0)
        camera!!.setDisplayOrientation(90)
    }

    override fun surfaceChanged(holder: SurfaceHolder, format: Int, w: Int, h: Int) {

        val params = camera!!.parameters
        camera!!.parameters = params

        val size = params.pictureSize
        previewHeight = size.height
        previewHeight = size.width

        try {
            camera!!.setPreviewCallback(cameraController)
            camera!!.setPreviewDisplay(holder)
        } catch (ex: Exception) {
            ex.printStackTrace()
        }

    }

    override fun surfaceDestroyed(surfaceHolder: SurfaceHolder) {
        camera!!.setPreviewCallback(null)
        camera!!.stopPreview()
        camera!!.release()
        camera = null
    }

    companion object {

        var previewWidth = 0
        var previewHeight = 0
    }

}
